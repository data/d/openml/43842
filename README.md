# OpenML dataset: League-of-Legend-High-Elo-Team-Comp--Game-Length

https://www.openml.org/d/43842

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I was exploring League of Legends datasets to play around but since Riot allows limited calls to their API, I've collected the data from OP.GG. 
Few goals of mine were to find out the best team compositions and to predict Victory/Loss given a team comp and game length. The most recent games on the dataset are on 2020 October 16th. 
Content
The dataset consists of ranked matches from Korea(WWW), North America(NA), Eastern Europe(EUNE), and Western Europe(EUW) servers. It has which team won the match, the total time of the match,  blue team composition and red team composition. Note that only the high elo games were added this includes Challenger, Grand Master, Master and sometimes even High Diamonds. Note that there are 151 total unique champions with 'Samira' as the latest addition.

You may find my blog post useful. (On quick data cleaning and analysis) https://leejaeka.github.io/jaekangai/fastpages/jupyter/2020/10/28/lolpredict.html

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43842) of an [OpenML dataset](https://www.openml.org/d/43842). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43842/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43842/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43842/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

